///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// File for List class
///
/// @file list.cpp
/// @version 1.0
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 08a - Cat Wranlger - EE 205 - Spr 2021
///date   15 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <stdlib.h>
#include <cassert>
#include "list.hpp"

using namespace std;

const bool DoubleLinkedList::empty() const {
   if ( head == nullptr )
      return true;
   else
      return 0;
}

void DoubleLinkedList::push_front( Node* newNode ) {
   if(newNode == nullptr)
      return;

   if( head == nullptr ) {
      newNode->prev = nullptr;
      newNode->next = nullptr;
      head = newNode;
      tail = newNode;
   }
   //if empty
   else{
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
   }
   
   count++;
   
}

void DoubleLinkedList::push_back(Node* newNode) {

   if (newNode == nullptr) {
      return;
   }

   //list isnt empty
   if (tail != nullptr) {
      newNode -> prev = tail;
      tail -> next = newNode;
      newNode -> next = nullptr;
      tail = newNode;
   } 

   //empty list heat tale are at same node
   else {
      newNode -> next = nullptr;
      newNode -> prev = nullptr;
      head = newNode;
      tail = newNode;
   }

   count++;
}

Node* DoubleLinkedList::pop_front() {

   if (head == nullptr) {
      return nullptr; 
   }

   
   if (head == tail) {
      Node* temp = head;
      head = nullptr;
      tail = nullptr;
      return temp;
   } 

   else { 
      Node* temp = head;
      head = head -> next;
      head -> prev = nullptr;
      temp -> next = nullptr;  
      count--; 
      return temp; 
   }
}


Node* DoubleLinkedList::pop_back() {
  
   if (tail == nullptr) {
      return nullptr; 
   }

   if (tail == head) {
      Node* temp = tail;
      head = nullptr;
      tail = nullptr;
      return temp;
   } 

   else { 
      Node* temp = tail;
      tail = tail -> prev;
      tail -> next = nullptr;
      temp -> prev = nullptr;  
      count--;
      return temp; 
   }
}


Node* DoubleLinkedList::get_first() const {
   return head;
}

Node* DoubleLinkedList::get_last() const {
   return tail;
}

Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   return currentNode->next;
}

Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
   return currentNode->prev;
}

void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
   if( currentNode == nullptr && head == nullptr){
      push_front(newNode);
      return;
   }
 
   if((currentNode != nullptr && head == nullptr) || (currentNode == nullptr && head     != nullptr)){
      assert(false);
   }
 
   if(currentNode == newNode || newNode == nullptr){
      return;
   }
 
   if (tail != currentNode){
      newNode->next = currentNode->next;
      currentNode->next = newNode;
      newNode->prev = currentNode;
      newNode->next->prev = newNode;
      count++;
   }
   else{
       push_back(newNode);
   }
}

void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
   if( currentNode == nullptr && head == nullptr){
      push_front(newNode);
      return;
   }

   if(currentNode == nullptr){
      push_back(newNode);
      return;
   }

   if((currentNode != nullptr && head == nullptr) || (currentNode == nullptr && head != nullptr)){
      assert(false);
   }

   if(currentNode == newNode && newNode == nullptr){
     return;
   }

   if (head != currentNode){
      newNode->prev = currentNode->prev;
      currentNode->prev->next = newNode;
      currentNode->prev = newNode;
      newNode->next = currentNode;
      count++;
   }
   else{
      push_front(newNode);
   }
}

void DoubleLinkedList::swap(Node* node1, Node* node2){
   //if node are the same or list empty or only one node in list
   if(node1 == node2 || empty() || size() == 1)
      return;

   //only two nodes in list
   if(size() == 2){
      if(node1 == head && node2 == tail){
         head = node2;
         tail = node2;
      }
      else if(node1 == tail && node2 ==head){
         head = node1;
         tail = node2;
      }

      head->prev = nullptr;
      head->next = tail;
      tail->next = nullptr;
      tail->prev = head;

   } 
   //more than 2 nodes
   else { 
         //both of the nodes are head or tail
         if((node1 == head && node2 == tail) || (node1 == tail && node2 == head)){
            if(node1 == head && node2 == tail){
               Node* next1 = head->next;
               Node* prev2 = tail->prev;

               head->next = nullptr;
               head->prev = prev2;
               prev2->next = head;
               tail->prev = nullptr;
               tail->next = next1;
               next1->prev = tail;
               head = node2;
               tail = node1;
            } 
            else if(node1 == tail && node2 == head){
               Node* next2 = head->next;
               Node* prev1 = tail->prev;

               head->next = nullptr;
               head->prev = prev1;
               prev1->next = head;
               tail->prev = nullptr;
               tail->next = next2;
               next2->prev = tail;
               head = node1;
               tail = node2;
            }

         //one of the node head or tail
         } 
         else if(node1 == head || node1 == tail || node2 == head || node2 == tail){
            Node* prev1 = node1->prev;
            Node* prev2 = node2->prev;
            Node* next1 = node1->next;
            Node* next2 = node2->next;

            //if nodes are adjacent or not
            if(node1 == head){
               if(node1->next == node2){
                  node1->next = next2;
                  node1->prev = node2;
                  next2->prev = node1;
                  node2->next = node1;
                  node2->prev = nullptr;
                  head = node2;
               } else {
                  node1->next = next2;
                  node1->prev = prev2;
                  next2->prev = node1;
                  prev2->next = node1;
                  node2->next = next1;
                  node2->prev = nullptr;
                  next1->prev = node2;
                  head = node2;
               }
            } 
            else if(node1 == tail){
               if(node2->next == node1){
                  node1->prev = prev2;
                  node1->next = node2;
                  prev2->next = node1;
                  node2->prev = node1;
                  node2->next = nullptr;
                  tail = node2;
               } else {
                  node1->prev = prev2;
                  node1->next = next2;
                  prev2->next = node1;
                  next2->prev = node1;
                  node2->prev = prev1;
                  node2->next = nullptr;
                  prev1->next = node2;
                  tail = node2;
               }
            } 
            else if(node2 == head){
               if(node2->next == node1){
                  node2->next = next1;
                  node2->prev = node1;
                  next1->prev = node2;
                  node1->next = node2;
                  node1->prev = nullptr;
                  head = node1;
               } 
               else {
                  node2->next = next1;
                  node2->prev = prev1;
                  next1->prev = node2;
                  prev1->next = node2;
                  node1->next = next2;
                  node1->prev = nullptr;
                  next2->prev = node1;
                  head = node1;
               }
            } 
            else if(node2 == tail){
               if(node1->next == node2){
                  node2->prev = prev1;
                  node2->next = node1;
                  prev1->next = node2;
                  node1->prev = node2;
                  node1->next = nullptr;
                  tail = node1;                
               } 
               else {
                  node2->prev = prev1;
                  node2->next = next1;
                  prev1->next = node2;
                  next1->prev = node2;
                  node1->prev = prev2;
                  node1->next = nullptr;
                  prev2->next = node1;
                  tail = node1;
               }
            }

         }
         //nodes are not head or tail
         else {
            Node* prev1 = node1->prev;
            Node* prev2 = node2->prev;
            Node* next1 = node1->next;
            Node* next2 = node2->next;
            
            bool nearHead1 = false;
            bool nearTail1 = false;
            bool nearHead2 = false;
            bool nearTail2 = false;

            if(node1->prev == head)
               nearHead1 = true;
            else if(node2->prev == head)
               nearHead2 = true;
            else if(node1->next == tail)
               nearTail1 = true;
            else if(node2->next == tail)
               nearTail2 = true;

            //adjacent nodes
            if(node1->next == node2){
               node1->next = next2;
               node1->prev = node2;
               next2->prev = node1;
               node2->next = node1;
               node2->prev = prev1;
               prev1->next = node2;
            } 
            else if(node2->next == node1){
               node2->next = next1;
               node2->prev = node1;
               next1->prev = node2;
               node1->next = node2;
               node1->prev = prev2;
               prev2->next = node1;
            } 
            // not adjacent
            else {
               node1->next = next2;
               node1->prev = prev2;
               next2->prev = node1;
               prev2->next = node1;
               node2->next = next1;
               node2->prev = prev1;
               prev1->next = node2;
               next1->prev = node2;
            }

            if(nearHead1)   
               head->next = node2;
            if(nearHead2)
               head->next = node1;
            if(nearTail1)
               tail->prev = node2;
            if(nearTail2)
               tail->prev = node1;

         }
   }
}

const bool DoubleLinkedList::isSorted() const {
   if( count <= 1 )  
      return true;
   for( Node* i = head; i->next != nullptr; i=i->next ) {
      if( *i > *i->next )
         return false;
   }
   return true;
}

void DoubleLinkedList::insertionSort(){
   if(isSorted())
      return;

   if(empty() || size() == 1) //if eimpty or one item
      return;

   for(Node* i = head; i->next != nullptr; i = i->next){    //outside
      Node* minNode = i;

      for(Node* j = i->next; j != nullptr; j = j->next){    //inside
         if(*minNode > *j)   //if new minimum is found in list
            minNode = j;   //update mininimum
      }
      swap(i, minNode);    //replace current node with smaller node found
      i = minNode;   //update node
   }
}

//test functions

bool DoubleLinkedList::validate() const{
   if(head== nullptr){
      assert (tail == nullptr);
      assert(empty());
      assert(size()==0);
   }
   else{
      assert(tail != nullptr);
      assert(!empty());
      assert(size()>0);
   }

   if(tail == nullptr){
      assert(head== nullptr);
      assert(empty());
      assert(size()==0);
   }
   else{
      assert(head != nullptr);
      assert(!empty());
      assert(size()>0);
   }

   if(head != nullptr && head == tail){
      assert(size()==1);
   }
      
   unsigned int countf = 0;
   Node* currentNode = head;
   while (currentNode != nullptr){
      countf++;

      if(currentNode ->next != nullptr){
         assert(currentNode ->next->prev == currentNode);
      }
      currentNode = currentNode->next;
   }

   assert(size() == countf);

   unsigned int countb = 0;
   currentNode = tail;
   while( currentNode != nullptr){
      countb++;

      if(currentNode->prev != nullptr){
         assert(currentNode->prev->next == currentNode);
      }
      currentNode = currentNode->prev;
   }
   assert(size() == countb);
   return true;

}


const bool DoubleLinkedList::isIn(Node* node) const{
      Node* currentNode =head;

      while(currentNode != nullptr){
         if(node == currentNode)
            return true;

         currentNode = currentNode->next;
      }
      return false;
}



