///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// Header file for List class
///
/// @file list.hpp
/// @version 1.0
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   15 Apr 2021
////////////////////////////////////////////////////////////////////////////

#pragma once
#include <string>
#include "node.hpp"

class DoubleLinkedList {
protected:
   Node* head = nullptr;
   Node* tail = nullptr;
   unsigned int count = 0;
public:
   const bool  empty() const;
   
   inline unsigned int size() const{
      return count;
   };

   void  push_front( Node* newNode );
   void  push_back( Node* newNode ); //Add newNode to the back of the list.
   
   Node* pop_front() ;
   Node* pop_back(); //Remove a node from the back of the list. If the list is already empty, return nullptr.
  
   Node* get_first() const;
   Node* get_last() const; //Return the very last node from the list. Don’t make any changes to the list.
   
   Node* get_next( const Node* currentNode ) const;
   Node* get_prev( const Node* currentNode ) const; //Return the node immediately before currentNode.
  
   void  insert_after( Node* currentNode, Node* newNode ); //Insert newNode immediately after currentNode.
   void  insert_before( Node* currentNode, Node* newNode ); //Insert newNode immediately before currentNode.

   void  swap( Node* node1, Node* node2 );
   const bool isSorted() const;
   void  insertionSort();

   bool validate() const;
   const bool isIn(Node* node) const;
   

};
